$('#contact-popup').on('click', function(e){
    var form = $('#contact-popup-form'),
        name = $(form).find('#contact-name').val(),
        email = $(form).find('#contact-email').val(),
        comments = $(form).find('#contact-comment').val(),
        g_recaptcha_response = $(form).find('#g-recaptcha-response').val(),
        validate = true;
    if (name === '') {
        $('#contact-name').parent('.box-field').addClass('no-validate');
        $('#contact-name').attr('placeholder', 'enter name');
        validate = false;
    } else {
      $('#contact-name').parent('.box-field').removeClass('no-validate');
      validate = true;
    };
    if (!validateEmail(email)) {
        $('#contact-email').parent('.box-field').addClass('no-validate');
        // console.log($('#email').parent('.form-group'));
        $('#contact-email').attr('placeholder', 'enter valide email');
        validate = false;
    };
    var data = {
        name: name,
        email: email,
        comments: comments,
        g_recaptcha_response: g_recaptcha_response
    }
    if (validate) {
        $.ajax({
            url: "php/send_email.php",
            method: "POST",
            dataType: 'JSON',
            data: data,
            success: function(html){
                console.log(html);
            },
            complete: function (res) {
                var textHtml = '<span class="succes-text">Information submitted. Thank You.</span>'
                $('#contact-popup-form').hide();
                $('#contact-popup-form').parent('.box-form').append(textHtml);
                setTimeout(function(){
                    $('.fancybox-close').trigger('click');
                }, 2000)
            },
            error: function(html){
              console.log('error');
            }
        });
    }
    e.preventDefault();
});

$('#started-popup').on('click', function(e){
    var form = $('#started-popup-form'),
        name = $(form).find('#started-name').val(),
        email = $(form).find('#started-email').val(),
        address = $(form).find('#started-address').val(),
        phone = $(form).find('#started-phone').val(),
        g_recaptcha_response = $(form).find('#g-recaptcha-response-1').val(),
        validate = true;
    
    if (name === '') {
        $('#started-name').parent('.box-field').addClass('no-validate');
        $('#started-name').attr('placeholder', 'enter name');
        validate = false;
    } else {
      $('#started-name').parent('.box-field').removeClass('no-validate');
      validate = true;
    };
    if (address === '') {
        $('#started-address').parent('.box-field').addClass('no-validate');
        $('#started-address').attr('placeholder', 'enter address');
        validate = false;
    } else {
      $('#started-address').parent('.box-field').removeClass('no-validate');
      validate = true;
    };
    if (phone === '') {
        $('#started-phone').parent('.box-field').addClass('no-validate');
        $('#started-phone').attr('placeholder', 'enter phone');
        validate = false;
    } else {
      $('#started-phone').parent('.box-field').removeClass('no-validate');
      validate = true;
    };
    if (!validateEmail(email)) {
        $('#started-email').parent('.box-field').addClass('no-validate');
        // console.log($('#email').parent('.form-group'));
        $('#started-email').attr('placeholder', 'enter valide email');
        validate = false;
    };
    var data = {
        name: name,
        email: email,
        address: address,
        phone: phone,
        g_recaptcha_response: g_recaptcha_response
    }
    if (validate) {
        $.ajax({
            url: "php/send_email-started.php",
            method: "POST",
            dataType: 'JSON',
            data: data,
            success: function(html){
                console.log(html);
            },
            complete: function (res) {
                var textHtml = '<span class="succes-text">Information submitted. Thank You.</span>'
                $('#started-popup-form').hide();
                $('#started-popup-form').parent('.box-form').append(textHtml);
                setTimeout(function(){
                    $('.fancybox-close').trigger('click');
                }, 2000)
            },
            error: function(html){
              console.log('error');
            }
        });
    }
    e.preventDefault();
});

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
