(function ($) {
  $(document).ready(function() {
    var wrapper = $('.blog-list');

    if ( wrapper.length ) {
      getFromFeed();
    }

  });

  function getFromFeed() {
    $.ajax({
      url: 'https://feeds.blogcertified.com/posts?type=order_desc&categories=215&num_of_posts=15',
      dataType: 'JSON',
      method: 'GET',
      data: {},
      complete: function (response) {
      },
      success: function (response) {
        var wrapper = $('.blog-list');

        if (response.data != undefined && response.data.length) {

          response.data.forEach(function(item, k) {
            var itemHtml = createNewsItem(item);
            $('.js-blog-slider').slick('slickAdd', itemHtml);
          });
        }
      },
      beforeSend: function () {
      },
      error: function (evt) {
      }
    });
  }

  function compareNumeric(a, b) {
    if (a.date < b.date) return 1;
    if (a.date > b.date) return -1;
  }

  function getShortContent(content) {
    var maxLength = 150; // maximum number of characters to extra

    if (content.length <= maxLength) {
      return content;
    }

    var trimmedString = content.substr(0, maxLength);

    trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" ")));
    trimmedString = trimmedString + '...';
    return trimmedString;
  }

  function createNewsItem(post) {
    var postLink = post.link,
        itemElem = $("<div></div>").addClass("blog-list__item"),
        itemDate = $("<div></div>").addClass("blog-list__item-date"),
        itemWrapp = $("<div></div>").addClass("blog-list__item-inf"),
        itemTitle = $("<a></a>").addClass("blog-list__item-name").append(post.title).attr('href', postLink).attr('target', '_blank'),
        itemText = $("<div></div>").addClass("blog-list__item-text").append(getShortContent(post.description)),
        itemReadMore = $("<a></a>").addClass("blog-list__item-more").append('read more').attr('href', postLink).attr('target', '_blank'),
        itemImgWrapp = $("<div></div>").addClass("blog-list__item-image"),
        itemImgLink = $("<a></a>").attr('href', postLink).attr('target', '_blank'),
        itemImage = $("<img>").attr('src', post.cached_thumbs['360x200']);

    var postDate = post.date.split(/[\s,]+/);

    if ( postDate[0] != undefined ) {
      postDate = postDate[0] + ' ' + postDate[1];
    }

    itemDate.append(postDate);

    itemImgLink.append(itemImage);
    itemImgWrapp.append(itemImgLink);

    itemWrapp.append(itemTitle);
    itemWrapp.append(itemText);
    itemWrapp.append(itemReadMore);

    itemElem.append(itemDate);
    itemElem.append(itemImgWrapp);
    itemElem.append(itemWrapp);

    return itemElem;
  }

})(jQuery);