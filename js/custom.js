$(window).load(function(){
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
		$('body').addClass('ios');
	} else{
		$('body').addClass('web');
	};
});
/* viewport width */
function viewport(){
	var e = window, 
		a = 'inner';
	if ( !( 'innerWidth' in window ) )
	{
		a = 'client';
		e = document.documentElement || document.body;
	}
	return { width : e[ a+'Width' ] , height : e[ a+'Height' ] }
};
/* viewport width */
$(function(){
	/* placeholder*/	   
	$('input, textarea').each(function(){
 		var placeholder = $(this).attr('placeholder');
 		$(this).focus(function(){ $(this).attr('placeholder', '');});
 		$(this).focusout(function(){			 
 			$(this).attr('placeholder', placeholder);  			
 		});
 	});
	/* placeholder*/

	// open/close menu
	$('.menu-btn').on('click', function(){
		$(this).toggleClass('active');
		$('.header-menu-wrapper').slideToggle();
		return false;
	});

	// open/close FAQ
	$('.faq-list__item').on('click', function(){
		$(this).find('.faq-list__item-text').slideToggle();
		$(this).siblings().find('.faq-list__item-text').slideUp()
		return false;
	});

	// close top bar 
 	$('.top-bar-close').on('click', function(){
 		$(this).parents('.top-bar').slideUp();
 		return false;
 	});

	// sticky menu
	$(document).on('scroll', function(){
		if ( $(document).scrollTop() > 5 ){
			$('header').addClass('sticky');
		} else{
			$('header').removeClass('sticky');
		}
	});

	if($('.js-industries-slider').length) {
		$('.js-industries-slider').slick({
			infinite: true,
			speed: 300,
			slidesToShow: 3,
			slidesToScroll: 1,
			appendArrows: '.industries-slider-btns',
			responsive: [
				{
				  breakpoint: 979,
				  settings: {
					slidesToShow: 2
				  }
				},
				{
				  breakpoint: 479,
				  settings: {
					slidesToShow: 1
				  }
				}			
			]
		});
	};

	if($('.js-blog-slider').length) {
		$('.js-blog-slider').slick({
			infinite: true,
			dots: true,
			speed: 300,
			slidesToShow: 3,
			slidesToScroll: 3,
			responsive: [
				{
				  breakpoint: 979,
				  settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				  }
				},
				{
				  breakpoint: 639,
				  settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				  }
				}			
			]
		});
	};


	if($('.fancybox').length) {
		$('.fancybox').fancybox({
			margin  : 30,
			padding  : 0
		});
	};


	if($('.scrollblock').length) {
		var drtn = 1500;
		var end1 = 164;
		var end2 = 210;
		var end3 = 294;
		if( viewport().height < 1000 ){
			drtn = 1100;
		}
		if( viewport().height < 1000 && viewport().width > 1250){
			drtn = 1100;
		}
		if( viewport().height > 1100 && viewport().width < 1200){
			drtn = 1800;
		}
		if( viewport().width > 1550){
			drtn = 1200;
		}
		if( viewport().width < 768){
			end1 = 120;
			end2 = 160;
			end3 = 187;
			drtn = 1400;
		}
		var scrollorama = $.scrollorama({
	        blocks:'.scrollblock'
	    });
	    scrollorama.animate('#start-image-1',{ delay: 200, duration: drtn, property:'left', start:1400, end: end1 });
	    scrollorama.animate('#start-image-2',{ delay: 300, duration: drtn, property:'left', start:1400, end: end2 });
	    scrollorama.animate('#start-image-3',{ delay: 400, duration: drtn, property:'left', start:1400, end: end3 });
	};

	if($('.scrollblock-1').length) {
		var drtn = 500;
		var start = 800;
		var property = 'right';
		var property1 = 'left';
		if( viewport().width < 1600 ){
			drtn = 350;
		}
		if( viewport().width < 1200 ){
			drtn = 900;
			start = 1200;
			property = 'top';
			property1 = 'top';
		}
		if( viewport().width < 900 ){
			drtn = 600;
		}
		if( viewport().width < 767 ){
			drtn = 400;
		}
		// if( viewport().height < 1000 && viewport().width > 1250){
		// 	drtn = 100;
		// }
		// if( viewport().height > 1100 && viewport().width < 1200){
		// 	drtn = 800;
		// }
		// if( viewport().width > 1550){
		// 	drtn = 200;
		// }
		// if( viewport().width < 768){
		// 	drtn = 400;
		// }
		var scrollorama = $.scrollorama({
	        blocks:'.scrollblock-1'
	    });
	    scrollorama.animate('#partner-image-1',{ delay: 600, duration: drtn, property:property, start:start, end: 0 });
	    scrollorama.animate('#partner-image-2',{ delay: 500, duration: drtn, property:property, start:start, end: 0 });
	    scrollorama.animate('#partner-image-3',{ delay: 400, duration: drtn, property:property, start:start, end: 0 });
	    scrollorama.animate('#partner-image-4',{ delay: 500, duration: drtn, property:property1, start:start, end: 0 });
	    scrollorama.animate('#partner-image-5',{ delay: 600, duration: drtn, property:property1, start:start, end: 0 });
	};

	if($('.wow').length) {
		new WOW().init();
	}

	
	/* components */

	/*

	$('.consult').parallaxie({
		speed: 0.99
	});
	
	if($('.styled').length) {
		$('.styled').styler();
	};
	if($('.fancybox').length) {
		$('.fancybox').fancybox({
			margon  : 10,
			padding  : 10
		});
	};
	if($('.scroll').length) {
		$(".scroll").mCustomScrollbar({
			axis:"x",
			theme:"dark-thin",
			autoExpandScrollbar:true,
			advanced:{autoExpandHorizontalScroll:true}
		});
	};
	
	*/
	
	/* components */
	
	

});

var handler = function(){
	
	var height_footer = $('footer').height();	
	var height_header = $('header').height();		
	//$('.content').css({'padding-bottom':height_footer+40, 'padding-top':height_header+40});
	
	
	var viewport_wid = viewport().width;
	var viewport_height = viewport().height;
	
	if (viewport_wid <= 991) {
		
	}

	// sticky menu 
	if ( $(document).scrollTop() > 5 ){
		$('header').addClass('sticky');
	} else{
		$('header').removeClass('sticky');
	}
	
}
$(window).bind('load', handler);
$(window).bind('resize', handler);



